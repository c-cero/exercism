defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    to_charlist(text)
    |> Enum.map(&(letter(&1,shift)))
    |> to_string
  end

  #def letter(a, shift) when a<?A or a>?z, do: a
  #def letter(a, shift) when a>?Z and a<?a, do: a
  def letter(a, shift) when a+shift>26, do: letter(a, shift-26)
  def letter(a, shift), do: a+shift
  
  def encrypt(char, shift) when char >= 65 and char <= 90, do: rem((char - 65 + shift),26) + 65
  def encrypt(char, shift) when char >= 97 and char <= 122, do: rem((char - 97 + shift),26) + 97
  def encrypt(char, shift), do: char
end