defmodule Pangram do

  @spec pangram?(String.t()) :: boolean
  def pangram?(sentence) do
    chars = String.downcase(sentence) |> String.to_charlist
    pangram(chars, Enum.to_list(?a..?z))
  end

  defp pangram([h|t], abc), do: pangram(t, abc -- [h])
  defp pangram([], []), do: true
  defp pangram([], _), do: false
end
