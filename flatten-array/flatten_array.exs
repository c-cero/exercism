defmodule FlattenArray do
  @doc """
    Accept a list and return the list flattened without nil values.

    ## Examples

      iex> FlattenArray.flatten([1, [2], 3, nil])
      [1,2,3]

      iex> FlattenArray.flatten([nil, nil])
      []

  """

  @spec flatten(list) :: list
  def flatten(list), do: flatten(list, [])

  defp flatten([], result), do: result
  defp flatten([nil|t], result), do: flatten(t, result)
  defp flatten([h|t], result) when is_list(h), do: flatten(t, result ++ flatten(h))
  defp flatten([h|t], result), do: flatten(t, result ++ [h])

end
