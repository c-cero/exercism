defmodule Roman do
  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t()
  def numerals(number) do
    num(number, "")
  end

  def num(n, acc) do
    cond do
      n >= 1000 -> num(n-1000, "M"<>acc)
      n >= 900 -> num(n-900, acc<>"CM")
      n >= 500 -> num(n-500, acc<>"D")
      n >= 400 -> num(n-400, acc<>"CD")
      n >= 100 -> num(n-100, acc<>"C")
      n >= 90 -> num(n-90, acc<>"XC")
      n >= 50 -> num(n-50, acc<>"L")
      n >= 40 -> num(n-40, acc<>"XL")
      n >= 10 -> num(n-10, acc<>"X")
      n >= 9 -> num(n-9, acc<>"IX")
      n >= 5 -> num(n-5, acc<>"V")
      n >= 4 -> num(n-4, acc<>"IV")
      n >= 1 -> num(n-1, acc<>"I")
      true -> acc
   end
  end
end
