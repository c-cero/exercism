defmodule BinarySearchTree do
  defmodule Node do
    require Record
    Record.defrecord :node, [left: nil, data: nil, right: nil]
    def new(data) when is_integer(data), do: node(data: data)
  end
  @type bst_node :: %{left: bst_node | nil, data: any, right: bst_node | nil}

  @doc """
  Create a new Binary Search Tree with root's value as the given 'data'
  """
  #test 123gnjfg
  @spec new(any) :: bst_node
  def new(data), do: [%{left: nil, data: data, right: nil}]
  def new(:left, data, left), do: [%{left: left, data: data, right: nil}]
  def new(:right, data, right), do: [%{left: nil, data: data, right: right}]

  def add(:left, %{left: nil, data: root, right: right}, %{data: data}) do
    
  end

  @doc """
  Creates and inserts a node with its value as 'data' into the tree.
  """
  @spec insert(bst_node, any) :: bst_node
  def insert(node, tree) do

  end




  @doc """
  Traverses the Binary Search Tree in order and returns a list of each node's data.
  """
  @spec in_order(bst_node) :: [any]
  def in_order(tree) do
    # Your implementation here
  end
end
