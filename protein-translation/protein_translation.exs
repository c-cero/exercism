defmodule ProteinTranslation do
  @proteins [UGU: "Cysteine", UGC: "Cysteine", UUA: "Leucine", UUG: "Leucine", AUG: "Methionine",
  UUU: "Phenylalanine", UUC: "Phenylalanine", UCU: "Serine", UCC: "Serine", UCA: "Serine",
  UCG: "Serine", UGG: "Tryptophan", UAU: "Tyrosine", UAC: "Tyrosine",
  UAA: "STOP", UAG: "STOP", UGA: "STOP"]
  @stoppers [:UAA, :UAG, :UGA]

  @doc """
  Given an RNA string, return a list of proteins specified by codons, in order.
  """
  @spec of_rna(String.t()) :: {atom, list(String.t())}
  def of_rna(rna) do
    toList(rna, [])
    |> toProtein([])
  end

  @doc """
  Given a codon, return the corresponding protein
  """
  @spec of_codon(String.t()) :: {atom, String.t()}
  def of_codon(codon) do
    case Keyword.fetch(@proteins, String.to_atom(codon)) do
      {:ok, result} = value -> value
      _ -> {:error, "invalid codon"}
    end
  end

  def toProtein([], names), do: {:ok, Enum.reverse(names)}
  def toProtein([codon|list], names) do
    case Keyword.fetch(@proteins,codon) do
      {:ok, result} -> toProtein(list, [result|names])
      _ -> {:error, "invalid RNA"}
    end
  end

  def toList("", list), do: Enum.reverse(list)
  def toList(rna, list) do
    {val, rest} = String.split_at(rna, 3)
    codon = String.to_atom(val)
    if stopCheck(codon), do: toList("", list), else: toList(rest, [codon|list])
  end

  def stopCheck(codon), do: Enum.any?(@stoppers, &(&1 == codon))
end
