defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t(), [String.t()]) :: [String.t()]
  def match(base, candidates) do
    for word <- candidates, anagram?(base, word), do: word
  end

  defp anagram?(a, b) do
    cond do
      String.downcase(a) == String.downcase(b) -> false
      true -> toList(a) == toList(b)
    end
  end

  def toList(word) do
    word |> String.downcase |> String.graphemes |> Enum.sort
  end 
end
