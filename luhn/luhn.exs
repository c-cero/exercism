defmodule Luhn do
  @doc """Checks if the given number is valid via the luhn formula"""
  @spec valid?(String.t()) :: boolean
  def valid?(number) do
    case properFormat(number) do
      true -> String.replace(number, " ", "")
        |> String.to_integer
        |> Integer.digits
        |> Enum.reverse
        |> Enum.with_index
        |> Enum.map(&formula/1)
        |> Enum.sum
        |> rem(10) == 0
      _ -> false
    end
  end

  defp properFormat(number), do: String.match?(number, ~r{^\d[\d\040]+$})

  defp formula({digit, index}) when rem(index, 2) == 0, do: digit
  defp formula({digit, _}) when digit * 2 > 9, do: digit * 2 -  9
  defp formula({digit, _}), do: digit * 2
end