defmodule Raindrops do
  @doc """
  Returns a string based on raindrop factors.

  - If the number contains 3 as a prime factor, output 'Pling'.
  - If the number contains 5 as a prime factor, output 'Plang'.
  - If the number contains 7 as a prime factor, output 'Plong'.
  - If the number does not contain 3, 5, or 7 as a prime factor,
    just pass the number's digits straight through.
  """
  @spec convert(pos_integer) :: String.t()
  def convert(number) do
    {_, result} = {number, ""}
    |> drop("Pling", 3)
    |> drop("Plang", 5)
    |> drop("Plong", 7)
    if result=="", do: Integer.to_string(number), else: result
  end

  def drop({num, text}, word, div) when rem(num, div)==0 do
    {num, text<>word}
  end
  def drop(num, _, _), do: num
end
